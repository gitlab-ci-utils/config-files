FROM node:22.14.0-alpine3.20@sha256:40be979442621049f40b1d51a26b55e281246b5de4e5f51a18da7beb6e17e3f9

WORKDIR /config

COPY . .

# hadolint ignore=DL3018
RUN apk update &&       \
  apk add --no-cache jq bash &&  \
  rm -rf /var/cache/apk/* && \
  chmod +x ./scripts/*.sh
