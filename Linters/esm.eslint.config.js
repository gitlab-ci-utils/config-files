import recommendedConfig from '@aarongoldenthal/eslint-config-standard/recommended-esm.js';

export default [
    ...recommendedConfig,
    {
        ignores: ['.vscode/**', 'archive/**', 'node_modules/**', 'coverage/**'],
        name: 'ignores'
    }
];
