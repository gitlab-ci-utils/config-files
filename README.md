# Config Files

A collection of default configuration files for standard tools (git, GitLab,
linters, editors) and scripts to create new projects.
