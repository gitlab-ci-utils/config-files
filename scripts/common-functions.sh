#!/bin/bash

# Function to copy files. Assumes JSON array with the following format,
# where src is relative to this project's root and dest is relative to the
# given destination folder:
#   [{ "src": "Git/.gitattributes", "dest": ".gitattributes" }]
function copy_files() {
    # Exit if required inputs not specified
    if [ -z "$1" ]; then
        echo "File list file name not specified"
        exit 1
    fi
    if [ -z "$2" ]; then
        echo "Destination directory not specified"
        exit 1
    fi

    # Pull list of source and destination files, and exit if not the same
    mapfile -t files_src < <(jq -r '.[] | .src' "$1")
    mapfile -t files_dest < <(jq -r '.[] | .dest' "$1")
    file_count_src=${#files_src[@]}
    file_count_dest=${#files_dest[@]}
    if [ "$file_count_src" -ne "$file_count_dest" ]; then
        echo "ERROR: Different source ($file_count_src) and destination ($file_count_dest) file counts"
        exit 1
    fi
    dest_dir="$2"

    source_dir="$(realpath "$(dirname "$0")")/.."
    counter=0
    while [ "$counter" -lt "$file_count_src" ]; do
        src_path="$source_dir/${files_src[counter]}"
        dest_path="$dest_dir/${files_dest[counter]}"
        echo "$src_path > $dest_path"
        cp "$src_path" "$dest_path"
        (( counter+=1 )) || true
    done
}

# Function to install npm modules. Assumes JSON array with the following format:
#   ["eslint", "jest"]
function install_modules() {
    # Exit if required inputs not specified
    if [ -z "$1" ]
    then
        echo "Module list file name not specified"
        exit 1
    fi
    if [ -z "$2" ]; then
        echo "Destination directory not specified"
        exit 1
    fi

    dest_dir="$2"
    origin=$(pwd)

    cd "$dest_dir" || exit
    # need word splitting to separate packages
    # shellcheck disable=SC2046
    npm install -D $(jq -r '.[]' "$1")
    cd "$origin" || exit
}

# Function to search for files with group/projet placeholders and replace them
# with the given values.
function update_group_project_placeholders() {
    # Exit if required inputs not specified
    if [ -z "$1" ]
    then
        echo "Destination directory not specified"
        exit 1
    fi
    if [ -z "$2" ]; then
        echo "Group name not specified"
        exit 1
    fi
    if [ -z "$3" ]; then
        echo "Project name not specified"
        exit 1
    fi

    dest_dir="$1"
    # Escape forward slashes in group name since sed delimeter
    new_group=$(echo "$2" | sed 's/\//\\\//g')
    new_project="$3"

    # Update files with group/project placeholders
    printf "\nUpdating group/project placeholders\n"
    group_placeholder="<group_name>"
    project_placeholder="<project_name>"
    mapfile -t placeholder_files < <(find "$dest_dir" -type f -exec grep -Eq "<group_name>|<project_name>" {} \; -print)
    for file in "${placeholder_files[@]}"; do
        if [ -f "$file" ]; then
            sed -i "s/$group_placeholder/$new_group/g" "$file"
            sed -i "s/$project_placeholder/$new_project/g" "$file"
            printf "Updated file: %s\n" "$file"
        fi
    done
}
