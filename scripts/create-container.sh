#!/bin/bash

# Gather project data from user
echo -n "Enter destination folder: "
read -r dest
echo -n "Enter group name: "
read -r group
echo -n "Enter project name: "
read -r project
echo

echo "Creating new project"
echo "Destination: $dest"
echo "Group: $group"
echo "Project: $project"

# Set default values
scripts_dir="$(realpath "$(dirname "$0")")"
default_files_json="$scripts_dir/default-files.json"
container_default_files_json="$scripts_dir/container-default-files.json"

# Include common functions
# shellcheck source=scripts/common-functions.sh
source "$scripts_dir/common-functions.sh"

# Create default folders, copy fails if they don't exist
new_directories=(
  ".gitlab"
  ".gitlab/issue_templates"
)
for directory in "${new_directories[@]}"; do
  mkdir -p "$dest/$directory"
done

# Copy default files
printf "\nCopying default files\n"
copy_files "$default_files_json" "$dest"

# Copy default container files
printf "\nCopying default npm files\n"
copy_files "$container_default_files_json" "$dest"

# Update files with group/project placeholders
update_group_project_placeholders "$dest" "$group" "$project"
