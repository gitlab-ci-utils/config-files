#!/bin/bash

# Gather project data from user
echo -n "Enter destination folder: "
read -r dest
echo -n "Enter group name: "
read -r group
echo -n "Enter project name: "
read -r project
echo -n "Include web features (y/n): "
read -r web
echo

echo "Creating new project"
echo "Destination: $dest"
echo "Group: $group"
echo "Project: $project"
echo "Web: $web"

# Set default values
scripts_dir="$(realpath "$(dirname "$0")")"
default_files_json="$scripts_dir/default-files.json"
npm_default_files_json="$scripts_dir/npm-default-files.json"
npm_web_files_json="$scripts_dir/npm-web-files.json"
npm_default_modules_json="$scripts_dir/npm-default-modules.json"
npm_web_modules_json="$scripts_dir/npm-web-modules.json"

# Include common functions
# shellcheck source=scripts/common-functions.sh
source "$scripts_dir/common-functions.sh"

# Create default folders, copy fails if they don't exist
new_directories=(
  ".gitlab"
  ".gitlab/issue_templates"
  ".vscode"
  "tests"
)
for directory in "${new_directories[@]}"; do
  mkdir -p "$dest/$directory"
done

# Copy default files
printf "\nCopying default files\n"
copy_files "$default_files_json" "$dest"

# Copy default npm package files
printf "\nCopying default npm files\n"
copy_files "$npm_default_files_json" "$dest"

# Copy web npm package files
if  [ "$web" == "y" ]; then
    # Create default ui test folder, copy fails if it doesn't exist
    mkdir -p "$dest"/tests-ui

    printf "\nCopying web npm package files\n"
    copy_files "$npm_web_files_json" "$dest"
fi

# Update files with group/project placeholders
update_group_project_placeholders "$dest" "$group" "$project"

# Install default packages
printf "\nInstalling default devDependencies\n"
install_modules "$npm_default_modules_json" "$dest"

# Install web packages
if  [ "$web" == "y" ]; then
    printf "\nInstalling web devDependencies\n"
    install_modules "$npm_web_modules_json" "$dest"
fi
